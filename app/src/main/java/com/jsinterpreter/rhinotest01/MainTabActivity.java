package com.jsinterpreter.rhinotest01;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainTabActivity extends AppCompatActivity implements CompilerFragment.SendMessage {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int level;
    private View decorView;
    private Menu menu_toolbar;

    //fragmento result
    private ResultFragment resultFragment=new ResultFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main_tab);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setTitle("Rhino Interpreter");
        setSupportActionBar(toolbar);

        //recuperando el fragmento

        /*
        if(savedInstanceState!=null){
            resultFragment = (ResultFragment)getSupportFragmentManager().getFragment(savedInstanceState,"result");
        }*/


        /* Crea soporte para navegacion [<-] */
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        level = getIntent().getIntExtra("level", 1);
        toolbar.setTitle("Lesson : " + (level+1));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        decorView = getWindow().getDecorView();
    }

    @Override
    public boolean onKeyDown(int key, KeyEvent keycode){
        super.onKeyDown(key, keycode);

        if(key==KeyEvent.KEYCODE_BACK){
            exit();
            return true;
        }else {
            return true;
        }

    }

    public int getLevel(){
        return level;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        //adapter.addFragment(new EditorFragment(), "EDITOR");

        ExplainFragment ex = new ExplainFragment();
        ex.setLevel(level);

        CompilerFragment co = new CompilerFragment();
        Bundle datos = new Bundle();
        co.setLevel(level);

        adapter.addFragment(ex,getResources().getString(R.string.title_tab_explain));
        adapter.addFragment(co,getResources().getString(R.string.title_tab_code));
        adapter.addFragment(resultFragment,getResources().getString(R.string.title_tab_result));
        //adapter.addFragment(new LogFragment(), "LOG");
        viewPager.setAdapter(adapter);
    }

    private void exit(){

        AlertDialog.Builder Dialogoadvertencia = new AlertDialog.Builder(this);
        Dialogoadvertencia.setTitle("Are you sure want to exit?");
        Dialogoadvertencia.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MainTabActivity.this.finish();
            }
        });
        Dialogoadvertencia.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog menuDrop = Dialogoadvertencia.create();
        menuDrop.show();
    }

    //imersive mode

    /*
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            );}
    }
    */

    @Override
    public void sendData(String msg) {
        resultFragment.getData(msg);
    }

    @Override
    public void sendError(int error) {
        resultFragment.getError(error);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);

        }

        @Override
        public Fragment getItem(int position) {

            /*doesn´t work
            if(position==1){
                menu_toolbar.findItem(R.id.action_delete).setEnabled(false);
            }else{
                menu_toolbar.findItem(R.id.action_delete).setEnabled(true);
            }*/
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return mFragmentTitleList.get(position);
        }
    }

    /* Action Bar Settings */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_tab, menu);
        menu_toolbar= menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.action_settings:

                break;
            case android.R.id.home:
                exit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
