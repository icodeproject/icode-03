package com.jsinterpreter.rhinotest01;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.util.Log;
import android.widget.Toast;

import net.jh.FileReader;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Jonathan on 8/5/2016.
 */
public class ManagerLevel {
    private static ManagerLevel instance = null;
    private ArrayList<String> listLevelsExplain;
    private ArrayList<String> listLevelsExercise;
    private ArrayList<String> listLevelsCode;
    private ArrayList<String> listQuestions;
    private ArrayList<String> listAnswers;
    //20 for max level
    //3 for each question
    //4 3 for answers and 1 for correct anser
    private String[][] Questions = new String[16][3];
    private String[][][] Answers =  new String[16][3][4];

    private int xp=15;//xp for level
    private int level_xp=0;
    private int []progressLimit={30,45,60,105};
    private int []visible_xp={30,75,135,240};
    private int current_xp=0;

    private String[] skills ={"Novice","Advanced","Expert","Hacker"};
    private int currentLevel=1;
    private int maxLevel=16;
    private Context context;
    private Resources resources;
    //to manage data saved
    private ArrayList<Level> levels;
    private ArrayList<String> contentCode;
    private SharedPreferences savedData;
    private SharedPreferences.Editor editSavedData;
    private String [][]data={
        //level , explain , code , quiz
        {"1","0","0","0"},//0
        {"0","0","0","0"},//1
        {"0","0","0","0"},//2
        {"0","0","0","0"},
        {"0","0","0","0"},
        {"0","0","0","0"},
        {"0","0","0","0"},
        {"0","0","0","0"},
        {"0","0","0","0"},
        {"0","0","0","0"},
        {"0","0","0","0"},//10
        {"0","0","0","0"},
        {"0","0","0","0"},
        {"0","0","0","0"},
        {"0","0","0","0"},
        {"0","0","0","0"},
        {"0","0","0","0"}//16
    };

    private String[] listSuccess={
            "0",//0
            "0",
            "0",
            "0",
            "0",
            "0",
            "0",
            "0",
            "0",
            "0",
            "0",
            "0",//11
            "0",
            "0",
            "0",
            "0",
            "0"//16
    };



    protected ManagerLevel(){

    }

    public static ManagerLevel getInstance(){
        if(instance==null){
            instance = new ManagerLevel();
        }
        return instance;
    }

    public void addXP(){
        current_xp+=15;
        if(current_xp>=progressLimit[level_xp]){
            if(level_xp<3){
                level_xp++;
                current_xp=0;
            }
        }
        editSavedData.putInt("LevelXP",level_xp);
        editSavedData.putInt("CurrentXP",current_xp);
        editSavedData.commit();
    }
    public String getSkill(){
        return skills[level_xp];
    }
    public int getXP(){
        return xp;
    }
    public int getProgressLimit(){
        return progressLimit[level_xp];
    }
    public int getLimitVisible(){
        return visible_xp[level_xp];
    }
    public int getLevelXP(){
        return level_xp;
    }
    public int getCurrentXP(){
        return current_xp;
    }
    public String getExplain(int level,int page){
        if(page==1){
            return listLevelsExplain.get(level);
        }else{
            return listLevelsExercise.get(level);
        }
    }

    public void setCurrentLevel(int lvl){
        currentLevel=lvl;
    }

    public int getCurrentLevel(){
        return currentLevel;
    }

    public String getCode(int level,int page){
        if(page==1) {
            return listLevelsCode.get(level);
        }else{
            return "";
        }
    }

    public String getQuestion(int level,int question){
        return Questions[level][question];
    }
    public String getAnswer(int level,int question,int pos){
        return Answers[level][question][pos];
    }

    public void setContext(Context c){
        context = c;
        resources = context.getResources();
    }

    public void init(){
        maxLevel = 16;//maximo de niveles
        listLevelsCode = new ArrayList<>();
        listLevelsExplain = new ArrayList<>();
        listLevelsExercise = new ArrayList<>();
        contentCode = new ArrayList<>();
        listAnswers = new ArrayList<>();
        listQuestions = new ArrayList<>();
        for(int i=0;i<maxLevel;i++){
            listLevelsExplain.add("file:///android_asset/EN/level"+(i+1+1)+".html");
            listLevelsExercise.add("file:///android_asset/EN/Training/level"+(i+1)+".html");
        }

        String script = "";

        for (int i = 0; i < 5 ; i++) {

            try {
                script = FileReader.readFromAssets(context, "scripts/lessons/lesson_level_" + String.valueOf(i + 1) + ".js");
                listLevelsCode.add(script);
            }
            catch (IOException e) {

            }
        }

        //listLevelsCode.add(context.getResources().getString(R.string.editorLevel2));
        //listLevelsCode.add(context.getResources().getString(R.string.editorLevel3));
        //listLevelsCode.add(context.getResources().getString(R.string.editorLevel4));
        //listLevelsCode.add(context.getResources().getString(R.string.editorLevel5));
        listLevelsCode.add(context.getResources().getString(R.string.editorLevel6));
        listLevelsCode.add(context.getResources().getString(R.string.editorLevel7));
        listLevelsCode.add(context.getResources().getString(R.string.editorLevel8));
        listLevelsCode.add(context.getResources().getString(R.string.editorLevel9));
        listLevelsCode.add(context.getResources().getString(R.string.editorLevel10));
        listLevelsCode.add(context.getResources().getString(R.string.editorLevel11));
        listLevelsCode.add(context.getResources().getString(R.string.editorLevel12));
        listLevelsCode.add(context.getResources().getString(R.string.editorLevel13));
        listLevelsCode.add(context.getResources().getString(R.string.editorLevel14));
        listLevelsCode.add(context.getResources().getString(R.string.editorLevel15));
        listLevelsCode.add(context.getResources().getString(R.string.editorLevel16));

        listQuestions.add(context.getResources().getString(R.string.questionLevel1));
        listQuestions.add(context.getResources().getString(R.string.questionLevel2));
        listQuestions.add(context.getResources().getString(R.string.questionLevel3));
        listQuestions.add(context.getResources().getString(R.string.questionLevel4));
        listQuestions.add(context.getResources().getString(R.string.questionLevel5));
        listQuestions.add(context.getResources().getString(R.string.questionLevel6));
        listQuestions.add(context.getResources().getString(R.string.questionLevel7));
        listQuestions.add(context.getResources().getString(R.string.questionLevel8));
        listQuestions.add(context.getResources().getString(R.string.questionLevel9));
        listQuestions.add(context.getResources().getString(R.string.questionLevel10));
        listQuestions.add(context.getResources().getString(R.string.questionLevel11));
        listQuestions.add(context.getResources().getString(R.string.questionLevel12));
        listQuestions.add(context.getResources().getString(R.string.questionLevel13));
        listQuestions.add(context.getResources().getString(R.string.questionLevel14));
        listQuestions.add(context.getResources().getString(R.string.questionLevel15));
        listQuestions.add(context.getResources().getString(R.string.questionLevel16));

        listAnswers.add(context.getResources().getString(R.string.answers_level1));
        listAnswers.add(context.getResources().getString(R.string.answers_level2));
        listAnswers.add(context.getResources().getString(R.string.answers_level3));
        listAnswers.add(context.getResources().getString(R.string.answers_level4));
        listAnswers.add(context.getResources().getString(R.string.answers_level5));
        listAnswers.add(context.getResources().getString(R.string.answers_level6));
        listAnswers.add(context.getResources().getString(R.string.answers_level7));
        listAnswers.add(context.getResources().getString(R.string.answers_level8));
        listAnswers.add(context.getResources().getString(R.string.answers_level9));
        listAnswers.add(context.getResources().getString(R.string.answers_level10));
        listAnswers.add(context.getResources().getString(R.string.answers_level11));
        listAnswers.add(context.getResources().getString(R.string.answers_level12));
        listAnswers.add(context.getResources().getString(R.string.answers_level13));
        listAnswers.add(context.getResources().getString(R.string.answers_level14));
        listAnswers.add(context.getResources().getString(R.string.answers_level15));
        listAnswers.add(context.getResources().getString(R.string.answers_level16));

        //llena el codigo de los niveles
        contentCode.add(context.getResources().getString(R.string.lvl1));
        contentCode.add(context.getResources().getString(R.string.lvl2));
        contentCode.add(context.getResources().getString(R.string.lvl3));
        contentCode.add(context.getResources().getString(R.string.lvl4));
        contentCode.add(context.getResources().getString(R.string.lvl5));
        contentCode.add(context.getResources().getString(R.string.lvl6));
        contentCode.add(context.getResources().getString(R.string.lvl7));
        contentCode.add(context.getResources().getString(R.string.lvl8));
        contentCode.add(context.getResources().getString(R.string.lvl9));
        contentCode.add(context.getResources().getString(R.string.lvl10));
        contentCode.add(context.getResources().getString(R.string.lvl11));
        contentCode.add(context.getResources().getString(R.string.lvl12));
        contentCode.add(context.getResources().getString(R.string.lvl13));
        contentCode.add(context.getResources().getString(R.string.lvl14));
        contentCode.add(context.getResources().getString(R.string.lvl15));
        contentCode.add(context.getResources().getString(R.string.lvl16));

        initQuestions();
        //loadData();
    }

    private void initQuestions(){
        for(int i=0;i<maxLevel;i++){
            Questions[i] = listQuestions.get(i).split("#");
            String []temp = listAnswers.get(i).split("#");
            for(int j=0;j<2;j++){
                Answers[i][j] = temp[j].split(",");
            }
        }
    }

    public void loadData(){
        levels = new ArrayList<>();
        savedData = context.getSharedPreferences("Data", Context.MODE_PRIVATE);
        editSavedData = savedData.edit();
        String d = savedData.getString("DataLevels","null");
        String s = savedData.getString("LevelsSuccess","null");
        level_xp = savedData.getInt("LevelXP",0);
        current_xp = savedData.getInt("CurrentXP",0);
        if(!d.equals("null")){
            String []lvls = d.split(";");
            for(int i=0;i<lvls.length;i++){
                String []booleans = lvls[i].split(",");
                data[i][0] = booleans[0];
                data[i][1] = booleans[1];
                data[i][2] = booleans[2];
                data[i][3] = booleans[3];
            }
        }
        if(!s.equals("null")){
            String []sc = s.split(",");
            for(int i=0;i<sc.length;i++){
                listSuccess[i] = sc[i];
            }
        }
        levels.add(new Level(resources.getString(R.string.list1).split("#")[0],resources.getString(R.string.list1).split("#")[1],1,cast(data[0][0]),cast(data[0][1]),cast(data[0][2]),cast(data[0][3])));
        levels.add(new Level(resources.getString(R.string.list2).split("#")[0],resources.getString(R.string.list2).split("#")[1],2,cast(data[1][0]),cast(data[1][1]),cast(data[1][2]),cast(data[1][3])));
        levels.add(new Level(resources.getString(R.string.list3).split("#")[0],resources.getString(R.string.list3).split("#")[1],3,cast(data[2][0]),cast(data[2][1]),cast(data[2][2]),cast(data[2][3])));
        levels.add(new Level(resources.getString(R.string.list4).split("#")[0],resources.getString(R.string.list4).split("#")[1],4,cast(data[3][0]),cast(data[3][1]),cast(data[3][2]),cast(data[3][3])));
        levels.add(new Level(resources.getString(R.string.list5).split("#")[0],resources.getString(R.string.list5).split("#")[1],5,cast(data[4][0]),cast(data[4][1]),cast(data[4][2]),cast(data[4][3])));
        levels.add(new Level(resources.getString(R.string.list6).split("#")[0],resources.getString(R.string.list6).split("#")[1],6,cast(data[5][0]),cast(data[5][1]),cast(data[5][2]),cast(data[5][3])));
        levels.add(new Level(resources.getString(R.string.list7).split("#")[0],resources.getString(R.string.list7).split("#")[1],7,cast(data[6][0]),cast(data[6][1]),cast(data[6][2]),cast(data[6][3])));
        levels.add(new Level(resources.getString(R.string.list8).split("#")[0],resources.getString(R.string.list8).split("#")[1],8,cast(data[7][0]),cast(data[7][1]),cast(data[7][2]),cast(data[7][3])));
        levels.add(new Level(resources.getString(R.string.list9).split("#")[0],resources.getString(R.string.list9).split("#")[1],9,cast(data[8][0]),cast(data[8][1]),cast(data[8][2]),cast(data[8][3])));
        levels.add(new Level(resources.getString(R.string.list10).split("#")[0],resources.getString(R.string.list10).split("#")[1],10,cast(data[9][0]),cast(data[9][1]),cast(data[9][2]),cast(data[9][3])));
        levels.add(new Level(resources.getString(R.string.list11).split("#")[0],resources.getString(R.string.list11).split("#")[1],11,cast(data[10][0]),cast(data[10][1]),cast(data[10][2]),cast(data[10][3])));
        levels.add(new Level(resources.getString(R.string.list12).split("#")[0],resources.getString(R.string.list12).split("#")[1],12,cast(data[11][0]),cast(data[11][1]),cast(data[11][2]),cast(data[11][3])));
        levels.add(new Level(resources.getString(R.string.list13).split("#")[0],resources.getString(R.string.list13).split("#")[1],13,cast(data[12][0]),cast(data[12][1]),cast(data[12][2]),cast(data[12][3])));
        levels.add(new Level(resources.getString(R.string.list14).split("#")[0],resources.getString(R.string.list14).split("#")[1],14,cast(data[13][0]),cast(data[13][1]),cast(data[13][2]),cast(data[13][3])));
        levels.add(new Level(resources.getString(R.string.list15).split("#")[0],resources.getString(R.string.list15).split("#")[1],15,cast(data[14][0]),cast(data[14][1]),cast(data[14][2]),cast(data[14][3])));
        levels.add(new Level(resources.getString(R.string.list16).split("#")[0],resources.getString(R.string.list16).split("#")[1],16,cast(data[15][0]),cast(data[15][1]),cast(data[15][2]),cast(data[15][3])));
        levels.add(new Level(resources.getString(R.string.list17).split("#")[0],resources.getString(R.string.list17).split("#")[1],17,cast(data[16][0]),cast(data[16][1]),cast(data[16][2]),cast(data[16][3])));
    }
    private void saveData(){
        String send="";
        for(int i=0;i<data.length;i++){
            send+=data[i][0]+","+data[i][1]+","+data[i][2]+","+data[i][3]+";";
        }
        editSavedData.putString("DataLevels", send);
        //save success
        String send2 ="";
        for(int i=0;i<listSuccess.length;i++){
            send2+=listSuccess[i]+",";
        }
        editSavedData.putString("LevelsSuccess",send2);
        editSavedData.commit();
    }


    public void setLevelSuccess(int lvl,boolean estado){
        this.levels.get(lvl).setSuccess(estado);
        data[lvl][0] = estado==true?"1":"0";
        saveData();
    }
    public void setPageSuccess(int lvl,int page,boolean estado){
        switch (page){
            case 0:
                this.levels.get(lvl).setPage1(estado);
                break;
            case 1:
                this.levels.get(lvl).setPage2(estado);
                break;
            case 2:
                this.levels.get(lvl).setPage3(estado);
                break;
        }
        data[lvl][page] = estado==true?"1":"0";
        saveData();
    }

    public Boolean containsCode(String text,int lvl){
        String codigolvl = contentCode.get(lvl);
        if(codigolvl.length()>0){
            String [] variables = codigolvl.split(",");
            for(int i=0;i<variables.length;i++){
                if(!text.contains(variables[i])){
                    return false;
                }
            }
            return true;
        }
        return true;
    }

    public boolean getLevelSuccess(int lvl){
        return cast(data[lvl][0]);
    }
    public boolean getPageSuccess(int lvl,int page){
        return cast(data[lvl][page]);
    }
    public boolean getSucces(int lvl){
        return cast(listSuccess[lvl]);
    }
    public void setSuccess(int lvl,Boolean value){
        listSuccess[lvl] = (value==true?"1":"0");
    }
    private boolean cast(String m){
        return m.equals("1") ==true;
    }

    public int getMaxLevel(){
        return maxLevel;
    }
    public ArrayList<Level> getList(){ return levels; }
    public int  LessonsSucess(){
        int s=0;
        for(int i=0;i<listSuccess.length;i++){
            if(cast(listSuccess[i])){
                s++;
            }
        }
        return s;
    }
    public int LessonProgress(){
        int p=0;
        for(int i=0;i<data.length;i++){
            for(int j=1;j<data[0].length;j++){
                if(cast(data[i][j])){
                    p++;
                }
            }
        }
        return p;
    }
}
