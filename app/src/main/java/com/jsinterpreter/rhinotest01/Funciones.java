package com.jsinterpreter.rhinotest01;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Home on 5/11/2015.
 */
public class Funciones {
    String txtDialogo;
    String texto;
    TextView lblTexto;
    EditText txtTexto;
    Context contexto;
    AlertDialog.Builder alert;
    //singleton
    private static Funciones instancia=null;
    public static Funciones getInstancia() {
        if(instancia == null) {
            instancia = new Funciones();
        }
        return instancia;
    }

    protected Funciones(){
        texto="";
        txtDialogo="";
        lblTexto=null;
        txtTexto=null;
        alert = null;
    }

    public void setContexto(Context c){
        contexto =c;
    }

    public void inicializarDialog(){
        //texto="";
        alert = new AlertDialog.Builder(contexto);
        final EditText input = new EditText(contexto);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        alert.setView(input);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                txtDialogo= input.getText().toString();
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

    }

    public void log(String dato){
        //texto=dato;
        if(lblTexto!=null){
            lblTexto.append(dato+System.getProperty("line.separator"));
        }
        if(txtTexto!=null){
            if(txtTexto.getText().toString().contains("Line #")){
                txtTexto.setText("");
            }
            txtTexto.append(dato + System.getProperty("line.separator"));
        }
    }

    public void prompt(String dato){
        if(alert!=null){
            inicializarDialog();
            alert.setTitle(dato);
            AlertDialog a = alert.show();

            if(a.isShowing()){
                try {
                    contexto.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }else{

            }
        }
    }

    public void setTextView(TextView textView){
        lblTexto=textView;
    }

    public void setEditText(EditText editText){
        txtTexto=editText;
    }
}
