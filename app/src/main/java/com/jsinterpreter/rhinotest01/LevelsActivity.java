package com.jsinterpreter.rhinotest01;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class LevelsActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;
    private int level;
    private View decorView;
    private Menu menu_toolbar;
    private SharedPreferences savedData;
    private SharedPreferences.Editor editSavedData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_levels);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("iCode");


        ManagerLevel.getInstance().setContext(this);
        ManagerLevel.getInstance().init();

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);





        savedData = getSharedPreferences("Data", Context.MODE_PRIVATE);
        editSavedData = savedData.edit();
        boolean initial = savedData.getBoolean("Initial",true);
        if(initial){
            Intent gotoMain = new Intent(this,LauncherActivity.class);
            startActivity(gotoMain);
        }
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        LevelsFragment frmLevel = new LevelsFragment();
        AboutFragment frmAbout = new AboutFragment();
        ProfileFragment frmProfile = new ProfileFragment();
        adapter.addFragment(frmLevel,"LEVELS");
        adapter.addFragment(frmProfile,"PROFILE");
        adapter.addFragment(frmAbout,"ABOUT");

        viewPager.setAdapter(adapter);
    }
    @Override
    public boolean onKeyDown(int key, KeyEvent keycode){
        super.onKeyDown(key, keycode);
        if(key==KeyEvent.KEYCODE_BACK){
            LevelsActivity.this.finish();
            return true;
        }else {
            return true;
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        String idiom = getResources().getConfiguration().locale.getLanguage().toLowerCase();
        /*
        if(idiom.equals("es")){
            Toast.makeText(this,"Español",Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this,"Otro idioma",Toast.LENGTH_LONG).show();
        }
        */
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }
        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
