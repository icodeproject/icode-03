package com.jsinterpreter.rhinotest01;

/**
 * Created by Jonathan on 8/5/2016.
 */
public class Level {
    private String name;
    private String description;
    private Boolean success;
    private Boolean page1;
    private Boolean page2;
    private Boolean page3;
    private Integer levelNumber;

    public Level() {
        setName("");
        setDescription("");
        setSuccess(false);
        setLevelNumber(0);
    }

    public Level(String name, String description, Integer levelNumber, Boolean success, Boolean page1, Boolean page2, Boolean page3) {
        this.name = name;
        this.description = description;
        this.success = success;
        this.page1 = page1;
        this.page2 = page2;
        this.page3 = page3;
        this.levelNumber = levelNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getLevelNumber() {
        return levelNumber;
    }

    public void setLevelNumber(Integer levelNumber) {
        this.levelNumber = levelNumber;
    }


    public Boolean getPage1() {
        return page1;
    }

    public void setPage1(Boolean page1) {
        this.page1 = page1;
    }

    public Boolean getPage2() {
        return page2;
    }

    public void setPage2(Boolean page2) {
        this.page2 = page2;
    }

    public Boolean getPage3() {
        return page3;
    }

    public void setPage3(Boolean page3) {
        this.page3 = page3;
    }
}
