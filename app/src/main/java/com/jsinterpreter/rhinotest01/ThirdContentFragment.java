package com.jsinterpreter.rhinotest01;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ThirdContentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ThirdContentFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ImageView imgComplete;
    private Button btnStart;
    private boolean aviable=false;
    private int level;


    public ThirdContentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ThirdContentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ThirdContentFragment newInstance(String param1, String param2) {
        ThirdContentFragment fragment = new ThirdContentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_third_content, container, false);
        btnStart = (Button)view.findViewById(R.id.btnStart);
        imgComplete = (ImageView)view.findViewById(R.id.imgComplete);
        level = getActivity().getIntent().getIntExtra("level", 1);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(aviable){
                    Intent gotoNext = new Intent(getContext(),QuizActivity.class);
                    gotoNext.putExtra("level",level);
                    gotoNext.putExtra("question",1);//star by 1
                    getContext().startActivity(gotoNext);
                }else{
                    Toast.makeText(getContext(),"Blocked",Toast.LENGTH_SHORT).show();
                }

            }
        });
        return view;
    }


    @Override
    public void onResume(){
        super.onResume();
        //if complete the firs page (Explain)
        if(ManagerLevel.getInstance().getPageSuccess(level,2)){
            aviable=true;
            btnStart.setText("Start");
        }else{
            aviable=false;
            btnStart.setText("Blocked");
        }
        if(ManagerLevel.getInstance().getPageSuccess(level,3)){
            imgComplete.setVisibility(View.VISIBLE);
            ManagerLevel.getInstance().setLevelSuccess(level+1,true);
        }
    }

}
