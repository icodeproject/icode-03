package net.jh;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.EditText;

import javax.xml.validation.Validator;

/**
 * Created by Jheovany on 25/04/2016.
 */
public class CodeEditor extends EditText {

    public CodeEditor(Context context, AttributeSet attrs) {

        super(context, attrs);

        setGravity(Gravity.TOP | Gravity.START);
        setTypeface(Typeface.MONOSPACE);
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        //setTextColor(Color.DKGRAY);


        //exampleCode();
    }

    @Override
    public void onDraw(Canvas c) {
        super.onDraw(c);

        //c.drawColor(Color.rgb(51, 204, 204));

        Paint p = new Paint();
        p.setFlags(Paint.ANTI_ALIAS_FLAG);
        p.setColor(Color.LTGRAY);

        c.drawRect(0, 0, getTextSize() * 2, c.getHeight() + getLineCount() * getLineHeight(), p);

        p.setColor(Color.BLACK);
        p.setTextSize(getTextSize() - 8);

        String[] lines = getText().toString().split("\n");

        Log.i("sl", String.valueOf(lines.length));

        int lineHeight = - 4;
        int lineNumber = 1;

        for (int i = 0; i < getLineCount(); i++) {

            float x = getTextSize();

            if ( lineNumber > 9 ) x -= getTextSize() - getTextSize() * 0.5 - 2;

            // Definir algoritmo de número de línea.

            c.drawText(String.valueOf(lineNumber), x, lineHeight += getLineHeight(), p);

            Layout layout = getLayout();
            int lineEnd = layout.getLineEnd(i);

            for (int j = i + 1; j < getLineCount(); j++) {
                if (getText().length() > 0 && getText().charAt(lineEnd - 1) != '\n') {
                    lineHeight += getLineHeight();
                    lineEnd = layout.getLineEnd(j);
                    i++;
                } else
                    break;
            }

            lineNumber++;
        }

        //p.setColor(Color.LTGRAY);
    }

    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {

       /* Log.i("Text Changed, text", text.toString());
        Log.i("Text Changed, start", String.valueOf(start));
        Log.i("Text Changed, lenghtB", String.valueOf(lengthBefore));
        Log.i("Text Changed, lenghtA", String.valueOf(lengthAfter));
        Log.i("selection", String.valueOf(getSelectionStart()));*/

        setHighlight();
    }

    private void setHighlight() {
        /*
        String[] keyWords = {"\nfunction ", " function ", "\nvar ", " var ", " return ",
            "\nfor ", " for ", "\nif ", " if ", "\nwhile ", " while ", "\ndo ", " do "};*/

        String code = getText().toString();

        String[] keyWords = {"var", "function", "return",
                "for", "if", "while", "do", "switch", "case", "default", "break","true","false"};

        getText().setSpan(new ForegroundColorSpan(Color.BLACK), 0, getText().length(), 0);
        //getText().setSpan(new StyleSpan(Typeface.NORMAL), 0, getText().length(), 0);

        //String[] splited = getText().toString().split("\\s+");
        String[] splited = getText().toString().split("\n");

        //Log.i("No of l", String.valueOf(splited.length));

        for (String keyWord : keyWords) {

            int offset = 0;

            for (int i = 0; i < getText().length(); i++) {

                int index = getText().toString().indexOf(keyWord, offset);

                if (index != -1) {

                    offset = index + keyWord.length();

                    if ((offset + 1) <= code.length()) {

                        Log.i("kw", String.valueOf(code.charAt(offset)));

                        if (index == 0 && ( code.charAt(offset) == '\u0020' ||
                                //code.length() == keyWord.length() ||
                                code.charAt(offset) == '(' ||
                                code.charAt(offset) == ':' ||
                                code.charAt(offset) == ';')) {

                            getText().setSpan(new ForegroundColorSpan(Color.rgb(0, 102, 153)), index, index + keyWord.length(), 0);
                        }

                        if (index > 0 &&
                                code.charAt(index - 1) == '\u0020' &&
                                ( code.charAt(offset) == '\u0020' ||
                                        code.charAt(offset) == '(' ||
                                        code.charAt(offset) == ':' ||
                                        code.charAt(offset) == ';')) {

                            getText().setSpan(new ForegroundColorSpan(Color.rgb(0, 102, 153)), index, index + keyWord.length(), 0);
                        }

                        if (index > 0 && code.charAt(index - 1) == '\n' &&
                                ( code.charAt(offset) == '\u0020' ||
                                        code.charAt(offset) == '(' ||
                                        code.charAt(offset) == ':' ||
                                        code.charAt(offset) == ';')) {

                            getText().setSpan(new ForegroundColorSpan(Color.rgb(0, 102, 153)), index, index + keyWord.length(), 0);
                        }
                    }
                }
            }
        }

        boolean found = false;

        // dqFound: double quote found
        boolean dqFound = false;

        //String code = getText().toString();

        int startString = 0;
        int endString = 0;

        int dqStartString = 0;
        int dqEndString = 0;

        int startComment = 0;
        int endComment = 0;

        for (int i = 0; i < code.length(); i++) {

            if (code.charAt(i) == '\'') {

                if (!found) {
                    startString = i;
                    found = true;
                }
                else {
                    endString = i;
                    found = false;
                    getText().setSpan(new ForegroundColorSpan(Color.rgb(0, 128, 0)), startString, endString + 1, 0);
                }
            }

            if (code.charAt(i) == '\"') {

                if (!dqFound) {
                    dqStartString = i;
                    dqFound = true;
                }
                else {
                    dqEndString = i;
                    dqFound = false;
                    getText().setSpan(new ForegroundColorSpan(Color.rgb(0, 128, 0)), dqStartString, dqEndString + 1, 0);
                }
            }
        }

        for (int i = 0; i < code.length() - 1; i++) {

            if (code.charAt(i) == '/' && code.charAt(i + 1) == '*') {
                //commentFound = true;
                startComment = i;

            }

            if (code.charAt(i) == '*' && code.charAt(i + 1) == '/') {
                //commentFound = false;
                endComment = i;
                getText().setSpan(new ForegroundColorSpan(Color.GRAY), startComment, endComment + 2, 0);
                //getText().setSpan(new StyleSpan(Typeface.ITALIC), startComment, endComment + 1, 0);
            }
        }

        int singleLineStartComment = 0;
        int singleLineEndComment = 0;

        for (int i = 0; i < code.length() - 1; i++) {

            if (code.charAt(i) == '/' && code.charAt(i + 1) == '/') {

                //Log.i("Single Comment", "found: " + String.valueOf(i));
                singleLineStartComment = i;
                singleLineEndComment = code.length();

                for (int j = i + 2; j < code.length() - 1; j++) {

                    if (code.charAt(j) == '\n') {//modificado sumado j + 1

                        Log.i("Line break", "found" + String.valueOf(j));
                        singleLineEndComment = j;//modificado
                        /*
                        if (code.charAt(j + 1) == '\n'){
                            getText().setSpan(new ForegroundColorSpan(Color.GRAY), singleLineStartComment, singleLineEndComment, 0);
                        }
                        */
                        //getText().setSpan(new ForegroundColorSpan(Color.GRAY), singleLineStartComment, singleLineEndComment, 0);
                        //getText().setSpan(new StyleSpan(Typeface.ITALIC), startComment, endComment + 1, 0);
                        i = j;
                        break;
                    }
                }

                getText().setSpan(new ForegroundColorSpan(Color.GRAY), singleLineStartComment, singleLineEndComment, 0);
            }
        }
    }

    private void exampleCode() {

        append("/* Sample JavaScript */");
        append("\nvar a = 4;");
        append("\nvar b = 3;");
        append("\nvar c = a + b;");
        append("\nconsole.log(c);");
        append("\n");
        append("\nfunction myFunction() {\n  return c;\n}");
        append("\n");
        append("\nconsole.log(\'A Message\');");

        setHighlight();
    }
}
