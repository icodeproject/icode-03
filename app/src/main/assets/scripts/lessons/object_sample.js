var person = {
  firstName: "John",
  lastName: "Doe",
  age: 24,
  eyeColor: "blue",
  fullName: function() {
    return this.firstName + " " + this.lastName;
  }
};