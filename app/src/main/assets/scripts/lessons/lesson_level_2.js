/* Data types */

// Number
var length = 16;

// Boolean
var result = true;

// String
var lastName = "Johnson";

// Array
var cars = ["Saab", "Volvo", "BMW"];

// Object
var x = {firstName:"John", lastName:"Doe"};

console.log('length: ' + length);
console.log('result: ' + result);
console.log('Last Name: ' + lastName);
console.log('car[0]: ' + cars[0]);
console.log('x.firstName: ' + x.firstName);