
var x = 10;
x += 5; // same as x = x + 5;
console.log(\'10+=5: \' + x);
var x = 10;
x -= 5; // same as x = x - 5;
console.log(\'10-=5: \' + x);
var x = 10;
x *= 5; // same as x = x * 5;
console.log(\'10*=5: \' + x);
var x = 10;
x /= 5; // same as x = x / 5;
console.log(\'10/=5: \' + x);
var x = 10;