# README #

We are pleased to announce that the app won second place in the IEEEmadC. :) 

iCode is an Application for Android to learn JavaScript through an interpreter.

This application was developed to participate in the contest [IEEEmadC](http://www.ieeemadc.org/index.php) (Mobile Applications Development Contest).